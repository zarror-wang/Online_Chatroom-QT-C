-- MySQL dump 10.13  Distrib 8.0.25, for Linux (x86_64)
--
-- Host: localhost    Database: Server_Msg_Info
-- ------------------------------------------------------
-- Server version	8.0.25-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Msg_Info`
--

DROP TABLE IF EXISTS `Msg_Info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Msg_Info` (
  `Msg_Id` int NOT NULL AUTO_INCREMENT,
  `Msg_Type` int NOT NULL,
  `Sender_Id` int NOT NULL,
  `Receiver_Id` int NOT NULL,
  `Normal_msg_Content` varchar(100) DEFAULT NULL,
  `File_msg_Content` mediumtext,
  `Msg_time` datetime(6) DEFAULT NULL,
  `File_msg_Name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Msg_Id`),
  KEY `FK_Sender_idx` (`Sender_Id`),
  KEY `FK_Receiver_idx` (`Receiver_Id`),
  CONSTRAINT `FK_Receiver` FOREIGN KEY (`Receiver_Id`) REFERENCES `User_Info` (`User_Inner_Id`),
  CONSTRAINT `FK_Sender` FOREIGN KEY (`Sender_Id`) REFERENCES `User_Info` (`User_Inner_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Msg_Info`
--

LOCK TABLES `Msg_Info` WRITE;
/*!40000 ALTER TABLE `Msg_Info` DISABLE KEYS */;
INSERT INTO `Msg_Info` VALUES (1,1,1,2,'hello',NULL,'2021-07-10 21:29:40.000000',NULL),(2,1,2,1,'你好',NULL,'2021-07-10 21:31:31.000000',NULL),(3,1,1,2,'今晚上吃啥？',NULL,'2021-07-10 21:33:33.000000',NULL),(4,1,2,1,'我这里有份表单，你看一下。',NULL,'2021-07-10 21:34:30.000000',NULL),(5,2,2,1,'','什么都不知道','2021-07-10 21:36:28.000000','menu.txt'),(9,1,1,2,'这是一个测试',NULL,'2021-07-13 01:52:11.000000',NULL),(16,2,1,2,NULL,'hello\nhello\nhello\nhello\nhello\nhello\nhello\nhello\nhello\nhelloplplpkp\nhellohellosaasdasdasd\nhello\nhellospdaksdijm\nhellosnadhoasjdasdjoans\nhello\nhello\nhelloasdnoahsodasdajsodasdnaks\nhello\nhellosadasfsd\nhello\nhello\nhello\nhello\n\n','2021-07-13 03:01:03.000000','1122.txt'),(17,1,1,2,'这是啥？',NULL,'2021-07-13 22:45:53.000000',NULL),(18,1,2,1,'我怎么知道?',NULL,'2021-07-14 00:22:12.000000',NULL),(19,1,1,2,'你不是很会？',NULL,'2021-07-14 00:22:55.000000',NULL),(20,1,2,1,'我不会',NULL,'2021-07-14 00:23:10.000000',NULL),(21,1,1,2,'我好累',NULL,'2021-07-14 01:36:06.000000',NULL),(22,1,1,5,'我爱你',NULL,'2021-07-14 01:55:13.000000',NULL),(23,1,5,1,'我不爱你',NULL,'2021-07-14 01:55:43.000000',NULL),(25,1,1,2,'你今天开心玛',NULL,'2021-07-14 02:33:18.000000',NULL),(26,1,2,1,'还好吧',NULL,'2021-07-14 02:33:33.000000',NULL),(29,1,1,2,'今天过得怎么样',NULL,'2021-07-14 23:48:25.000000',NULL),(30,1,2,1,'还可以',NULL,'2021-07-14 23:48:39.000000',NULL),(31,2,1,2,NULL,'这是一个测试文件，创建于时间07142349\n','2021-07-14 23:50:10.000000','test.txt'),(32,1,1,2,'结束',NULL,'2021-07-14 23:51:12.000000',NULL);
/*!40000 ALTER TABLE `Msg_Info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Relationship_Info`
--

DROP TABLE IF EXISTS `Relationship_Info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Relationship_Info` (
  `Relationship_Id` int NOT NULL,
  `A_Id` int NOT NULL,
  `B_Id` int NOT NULL,
  PRIMARY KEY (`Relationship_Id`),
  KEY `B_id_FK_idx` (`B_Id`),
  KEY `A_Id_idx` (`A_Id`),
  CONSTRAINT `A_id_FK` FOREIGN KEY (`A_Id`) REFERENCES `User_Info` (`User_Inner_Id`),
  CONSTRAINT `B_id_FK` FOREIGN KEY (`B_Id`) REFERENCES `User_Info` (`User_Inner_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Relationship_Info`
--

LOCK TABLES `Relationship_Info` WRITE;
/*!40000 ALTER TABLE `Relationship_Info` DISABLE KEYS */;
INSERT INTO `Relationship_Info` VALUES (1,1,2),(2,1,5),(3,1,8),(4,2,1),(5,5,1),(6,8,1),(7,2,4),(8,4,2);
/*!40000 ALTER TABLE `Relationship_Info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `User_Group_Relation`
--

DROP TABLE IF EXISTS `User_Group_Relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `User_Group_Relation` (
  `User_Group_Relation_Id` int NOT NULL,
  `Group_Id` varchar(45) NOT NULL,
  `Group_Name` varchar(45) NOT NULL,
  `User_Id` varchar(45) NOT NULL,
  `User_Inner_Id` int DEFAULT NULL,
  PRIMARY KEY (`User_Group_Relation_Id`),
  KEY `FK_User_idx` (`User_Id`),
  KEY `FK_User_Id_idx` (`User_Inner_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User_Group_Relation`
--

LOCK TABLES `User_Group_Relation` WRITE;
/*!40000 ALTER TABLE `User_Group_Relation` DISABLE KEYS */;
INSERT INTO `User_Group_Relation` VALUES (1,'1','Group_A','1120180001',1),(2,'1','Group_A','1120180002',2),(3,'1','Group_A','1120180003',3),(4,'2','Group_B','1120180007',7),(5,'2','Group_B','1120180008',8),(6,'2','Group_B','1120180009',9);
/*!40000 ALTER TABLE `User_Group_Relation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `User_Info`
--

DROP TABLE IF EXISTS `User_Info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `User_Info` (
  `User_Inner_Id` int NOT NULL,
  `User_Name` varchar(45) NOT NULL,
  `User_Passwd` varchar(45) NOT NULL,
  `User_Id` varchar(45) NOT NULL,
  PRIMARY KEY (`User_Inner_Id`),
  UNIQUE KEY `User_Id_UNIQUE` (`User_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User_Info`
--

LOCK TABLES `User_Info` WRITE;
/*!40000 ALTER TABLE `User_Info` DISABLE KEYS */;
INSERT INTO `User_Info` VALUES (1,'Zarror','123','1120180001'),(2,'Mike','123','1120180002'),(3,'Lisa','123','1120180003'),(4,'Perl','123','1120180004'),(5,'Cooper','123','1120180005'),(6,'Bob','123','1120180006'),(7,'Carl','123','1120180007'),(8,'Tim','123','1120180008'),(9,'David','123','1120180009');
/*!40000 ALTER TABLE `User_Info` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-07-15  1:03:41
