#include "server_thread.h"
#include <QTcpSocket>
#include <QDebug>
#include <QtSql/QtSql>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlRecord>

server_thread::server_thread(QObject *parent,QTcpSocket* tcp) : QThread(parent)
{
    m_tcp=tcp;

}

void server_thread::run()
{
    //检测是否可以接受数据
    connect(m_tcp,&QTcpSocket::readyRead,this,[=](){
        req_str *oral_req_str=new req_str;
        m_tcp->read((char*)oral_req_str,sizeof(req_str));
        qDebug()<<"收到请求，请求号为"<<oral_req_str->server_type;
        fun_center(oral_req_str);
    });
    connect(m_tcp,&QTcpSocket::disconnected,this,[=](){
        m_tcp->close();
        m_tcp->deleteLater();
    });
}

void server_thread::fun_center(req_str *req)//服务分发中心
{
    //    QPair<QString ,QString > user_info;
    //    QVector<QPair<QString ,QString >> userlist;
    //    userlist.push_back(QPair<QString,QString>("王泽龙","1120182199"));
    //    userlist.push_back(QPair<QString,QString>("王一法","1120182199"));
    //    userlist.push_back(QPair<QString,QString>("瑷珲","1120182199"));
    //    userlist.push_back(QPair<QString,QString>("朴知晓","1120182199"));
    //    userlist.push_back(QPair<QString,QString>("木木","1120182199"));
    //    userlist.push_back(QPair<QString,QString>("小和","1120182199"));
    //    userlist.push_back(QPair<QString,QString>("三地","1120182199"));
    //    userlist.push_back(QPair<QString,QString>("禽湖","1120182199"));
    //    userlist.push_back(QPair<QString,QString>("阿咯","1120182199"));
    //    qDebug()<<"受到请求，服务类型为"<<req->server_type;

    //    ack_str* ack=new ack_str;
    //    if(req->server_type==2)
    //    {
    //        ack->server_type=2;
    //        ack->user_cont=userlist.size();
    //        //从数据库获取好友姓名和uid
    //        for(int i=0;i<userlist.size();i++)
    //        {
    //            strcpy(ack->userlist[i][0],userlist[i].first.toUtf8());
    //            strcpy(ack->userlist[i][1],userlist[i].second.toUtf8());
    //        }
    //        m_tcp->write((char*) ack,sizeof(ack_str));
    //    }
    switch (req->server_type) {
    case 1://身份验证服务
        fun_id_authorication(req);
        break;
    case 2://获取好友列表
        fun_get_userlist(req);
        break;
    case 3://获取用户所在群聊信息
        fun_get_grouplist(req);
        break;
    case 4://获取用户聊天记录
        fun_get_meglog(req);
        break;
    case 5://插入普通消息
        fun_insert_normal_meg(req);
        break;
    case 6://插入文件信息
        fun_insert_file_meg(req);
        break;
    case 7://获取文件信息
        fun_get_file_meg(req);
        break;


    }

}


void server_thread::fun_get_file_meg(req_str* req)//获取文件信息
{
    qDebug()<<"请求聊天文件";
    QSqlQuery query;
    QString buffer;//保存文件的临时变量
    QString sel_line;//查询语句文本
    sel_line+="select * from Msg_Info where File_msg_Name = '";
    sel_line.append(req->file_name);
    sel_line.append("'");
    qDebug()<<"聊天文件查询语句"<<sel_line;
    query.exec(QObject::tr(sel_line.toUtf8()));
    QSqlRecord rec=query.record();
    while(query.next())
    {
        buffer=query.value(rec.indexOf("File_msg_Content")).toString();
    }

    ack=new ack_str;
    ack->server_type=7;//设置服务号
    strcpy(ack->file_meg_con,buffer.toUtf8());
    qDebug()<<buffer;
    m_tcp->write((char*)ack,sizeof(ack_str));
}


void server_thread::fun_insert_file_meg(req_str* req)//插入文件消息
{
    QSqlQuery query;
    QString inse_line;//插入语句文本
    inse_line+="Insert into Msg_Info (Msg_Type, Sender_Id,Receiver_Id,File_msg_Content,Msg_time,File_msg_Name) Values ('2','";
    inse_line+=req->user_id[9];
    inse_line+="','";
    inse_line+=req->log_tar_u_id[9];
    inse_line+="','";
    inse_line.append(req->file_meg);
    inse_line+="','";
    inse_line.append(req->time_stamp);
    inse_line+="','";
    inse_line.append(req->file_name);
    inse_line+="');";
    qDebug()<<"插入语句为"<<inse_line;
    //执行插入语句
    query.prepare(inse_line);
    ack=new ack_str;
    ack->server_type=6;//服务号
    //插入结果返回并打印
    if(!query.exec())
    {
        qDebug()<<"文件插入失败"<<query.lastError();
        ack->insert_file_flag=0;//记录失败
    }
    else
    {
        qDebug()<<"文件插入成功";
        ack->insert_file_flag=1;//记录成共
    }
    //向客户端返回执行结果
    m_tcp->write((char*)ack,sizeof(ack_str));
}

void server_thread::fun_insert_normal_meg(req_str* req)//插入普通消息
{
    QSqlQuery query;
    QString inse_line;//插入语句文本
    inse_line+="Insert into Msg_Info (Msg_Type, Sender_Id,Receiver_Id,Normal_msg_Content,Msg_time) Values ('1','";
    inse_line+=req->user_id[9];
    inse_line+="','";
    inse_line+=req->log_tar_u_id[9];
    inse_line+="','";
    inse_line.append(req->normal_meg);
    inse_line+="','";
    inse_line.append(req->time_stamp);
    inse_line+="');";
    qDebug()<<"插入语句为"<<inse_line;
    //执行插入语句
    query.prepare(inse_line);
    ack=new ack_str;
    ack->server_type=5;//服务号

    //插入结果返回并打印
    if(!query.exec())
    {
        qDebug()<<"插入失败"<<query.lastError();
        ack->insert_normal_flag=0;//记录成功
    }
    else
    {
        qDebug()<<"插入成功";
        ack->insert_normal_flag=1;//记录失败
    }
    //向客户端返回执行结果
    m_tcp->write((char*)ack,sizeof(ack_str));
}

void server_thread::fun_get_meglog(req_str* req)//获取与单个用户的聊天记录
{
    int meg_type[100]={0};//记录消息的类型
    QVector<QPair<QString,QString>> meg_list;//记录消息的(时间，内容)（文件名，文件消息序列号）
    //数据库查询元语；
    QSqlQuery query;
    QString sel_line;//查找语句文本
    qDebug()<<"请求聊天记录，验证信息为"<<req->user_id<<req->log_tar_u_id;
    sel_line+="select * from Msg_Info where ((Sender_Id=";
    sel_line.append(req->user_id[9]);
    sel_line+=" and Receiver_Id=";
    sel_line.append(req->log_tar_u_id[9]);
    sel_line+=") or (Sender_Id= ";
    sel_line.append(req->log_tar_u_id[9]);
    sel_line+=" and Receiver_Id=";
    sel_line.append(req->user_id[9]);
    sel_line+=")) order by Msg_time";
    qDebug()<<"聊天记录查询语句"<<sel_line;
    query.exec(QObject::tr(sel_line.toUtf8()));
    QSqlRecord rec=query.record();
    QString msg_type;//消息类型
    QString msg_index;//消息序列号
    QString msg_timestamp;//消息时间辍
    QString normal_meg_con;//普通消息内容
    QString File_meg_index;//文件消息序列号
    QString sender_tag;//发送方标记
    QString file_name;//文件消息文件名
    int msg_cont=0;
    while(query.next())
    {
        msg_type=query.value(rec.indexOf("Msg_Type")).toString();//获取该消息类型
        msg_index=query.value(rec.indexOf("Msg_Id")).toString();//获取该消息的序列号
        if(msg_type=="1")//是条普通消息
        {
            normal_meg_con=query.value(rec.indexOf("Normal_msg_COntent")).toString();//获取标准信息内容
            sender_tag=query.value(rec.indexOf("Sender_Id")).toString();//获取信息发送方标识
            msg_timestamp=query.value(rec.indexOf("Msg_time")).toString();//获取消息时间
            if(sender_tag==req->user_id[9])
                meg_type[msg_cont]=2;//自己发送的正常消息
            else
                meg_type[msg_cont]=1;//别人发送的正常消息
            meg_list.push_back(QPair<QString,QString>(msg_timestamp,normal_meg_con));//记录消息时间与内容
        }
        else if(msg_type=="2")
        {
            file_name=query.value(rec.indexOf("File_msg_Name")).toString();//获取文件名
            File_meg_index=query.value(rec.indexOf("Msg_Id")).toString();//获取文件信息的信息序列号
            sender_tag=query.value(rec.indexOf("Sender_Id")).toString();//获取信息发送方标识
            //msg_timestamp=query.value(rec.indexOf("")).toString();//获取消息时间
            if(sender_tag==req->user_id[9])
                meg_type[msg_cont]=4;//自己发送的文件
            else
                meg_type[msg_cont]=3;//别人发送的文件
            meg_list.push_back(QPair<QString,QString>(file_name,File_meg_index));//记录文件名与文件消息序列号
        }
        msg_cont++;
    }
    //从数据库获取信息完成，现在打包放入ack
    ack=new ack_str;
    strcpy(ack->client_id,req->user_id);
    strcpy(ack->user_meglog_u_id,req->log_tar_u_id);
    ack->server_type=4;
    ack->user_meglog_cont=msg_cont;//记录信息数量
    for(int i=0;i<msg_cont;i++)
        ack->user_meglog_type[i]=meg_type[i];//记录信息类型
    for(int i=0;i<msg_cont;i++)
    {
        strcpy(ack->user_meglog[i][0],meg_list[i].first.toUtf8());//保存消息时间辍或文件名
        strcpy(ack->user_meglog[i][1],meg_list[i].second.toUtf8());//保存消息内容或文件消息序列号
    }
    m_tcp->write((char*)ack,sizeof(ack_str));
}

void server_thread::fun_id_authorication(req_str *req)//用户登陆函数
{
    //数据库查询元语；
    QSqlQuery query;
    QString sel_line;//查找语句文本
    qDebug()<<"请求验证，验证信息为"<<req->user_id<<req->user_passwd;
    sel_line+="select * from User_Info where User_Id=";
    sel_line.append(req->user_id);
    qDebug()<<sel_line;
    query.exec(QObject::tr(sel_line.toUtf8()));
    QSqlRecord rec=query.record();
    QString base_passwd;
    QString clienter_name;

    while(query.next())
    {
        base_passwd=query.value(rec.indexOf("User_Passwd")).toString();//获取到登陆用户名的密码
        clienter_name=query.value(rec.indexOf("User_Name")).toString();//获取到登陆用户名的name
        qDebug()<<base_passwd<<clienter_name;
    }
    if(req->user_passwd==base_passwd)//密码验证成功
    {
        ack=new ack_str;
        ack->server_type=1;//服务号
        ack->log_flag=1;
        strcpy(ack->client_name,clienter_name.toUtf8());
        m_tcp->write((char*)ack,sizeof(ack_str));
    }
    else
    {
        ack=new ack_str;
        ack->server_type=1;//服务号
        ack->log_flag=0;
        m_tcp->write((char*)ack,sizeof(ack_str));
    }
}

void server_thread::fun_get_userlist(req_str *req)//获取用户的好友列表
{
    qDebug()<<"请求好友列表";
    QVector<QPair<QString ,QString >> userlist;//用户信息列表
    QVector<QString> B_id_list;//用户好友的内部id集合
    QSqlQuery query;
    QString sel_line;//查找语句文本
    sel_line+="select B_Id from Relationship_Info where A_Id=";
    sel_line.append(req->user_id[9]);
    qDebug()<<sel_line;
    query.exec(QObject::tr(sel_line.toUtf8()));
    QSqlRecord rec=query.record();
    while(query.next())
    {
        QString B_id=query.value(rec.indexOf("B_id")).toString();
        B_id_list.push_back(B_id);
    }
    query.clear();//晴空
    rec.clear();//晴空
    QString temp_b_id_line;//临时构造好友内部id的集合
    for(int i=0;i<B_id_list.size()-1;i++)
    {
        temp_b_id_line.append(B_id_list[i]);
        temp_b_id_line.append(',');
    }
    if(B_id_list.size()>0)
        temp_b_id_line.append(B_id_list[B_id_list.size()-1]);
    temp_b_id_line.append(')');
    sel_line.clear();
    sel_line="select User_Name, User_Id from User_Info where User_Inner_Id in (";
    sel_line.append(temp_b_id_line);//构造完成最终查询语句
    qDebug()<<sel_line;
    query.exec(QObject::tr(sel_line.toUtf8()));
    rec=query.record();
    while(query.next())
        userlist.push_back( QPair<QString ,QString >(query.value(rec.indexOf("User_Name")).toString(),query.value(rec.indexOf("User_Id")).toString()));
    for(int i=0;i<userlist.size();i++)
        qDebug()<<userlist[i].first<<userlist[i].second;
    ack=new ack_str;
    ack->server_type=2;
    ack->user_cont=userlist.size();
    //从数据库获取好友姓名和u_id
    for(int i=0;i<userlist.size();i++)
    {
        strcpy(ack->userlist[i][0],userlist[i].first.toUtf8());
        strcpy(ack->userlist[i][1],userlist[i].second.toUtf8());
    }
    m_tcp->write((char*) ack,sizeof(ack_str));
}

void server_thread::fun_get_grouplist(req_str *req)//获取用户群聊列表
{
    qDebug()<<"请求群聊列表，验证信息为"<<req->client_name;
    QVector<QPair<QString,QString>> grouplist;//群聊信息，包括群聊名称，群聊id
    QVector<QVector<QString>> group_member_info;//每个群聊包含的用户的姓名
    QVector<QString> a;//用于初始化的无意义参数
    for(int i=0;i<20;i++)//初始压入，以便实现后续可以根据下标访问
        grouplist.push_back(QPair<QString,QString>("",""));
    for(int i=0;i<20;i++)//初始压入，以便实现后续可以根据下标访问
        group_member_info.push_back(a);
    int total_group_cont=0;
    int each_group_mem_cont[20]={0};
    QSqlQuery query;
    QString sel_line;//查找语句文本
    QSqlQuery query1;
    QString sel_line1;//查找语句文本
    QSqlRecord rec;
    QSqlRecord rec1;
    sel_line+="select Group_Id, Group_Name from User_Group_Relation where User_Inner_Id=";
    sel_line.append(req->user_id[9]);
    qDebug()<<sel_line;
    query.exec(QObject::tr(sel_line.toUtf8()));
    rec=query.record();
    while(query.next())
    {
        QString temp_group_id=query.value(rec.indexOf("Group_Id")).toString();//该用户所在的每一个群聊的id号
        QString temp_group_name=query.value(rec.indexOf("Group_Name")).toString();//该用户所在的每一个群聊的id号
        grouplist[total_group_cont].first=temp_group_name;//保存群聊名称
        grouplist[total_group_cont].second=temp_group_id;//保存群聊id
        sel_line1+="select User_Name from User_Info where User_Inner_Id in (select User_Inner_Id from User_Group_Relation where Group_Id =";
        sel_line1+=temp_group_id;
        sel_line1+=')';
        qDebug()<<sel_line1;
        query1.exec(QObject::tr(sel_line1.toUtf8()));
        rec1=query1.record();
        while(query1.next())
        {
            QString temp_memin_group=query1.value(rec1.indexOf("User_Name")).toString();//获取该群组下的每个用户名
            group_member_info[total_group_cont].push_back(temp_memin_group);//针对该群记录一个成员名
            each_group_mem_cont[total_group_cont]++;//该群的群聊人数加一
        }
        total_group_cont++;
    }
    //    for(int i=0;i<total_group_cont;i++)
    //    {
    //        qDebug()<<"群聊名称--"<<grouplist[i].first<<"群聊id--"<<grouplist[i].second<<"群聊人数---"<<each_group_mem_cont[i];
    //        for(int j=0;j<each_group_mem_cont[i];j++)
    //            qDebug()<<"用户--->"<<group_member_info[i][j];
    //    }
    //至此已经准备好群聊数据了，现在放入ack中
    ack=new ack_str;
    ack->server_type=3;//返回类型
    ack->Group_cont=total_group_cont;
    for(int i=0;i<grouplist.size();i++)
    {
        strcpy(ack->Grouplist[i][0],grouplist[i].first.toUtf8());//放入群聊名称
        strcpy(ack->Grouplist[i][1],grouplist[i].second.toUtf8());//放入群聊id
        ack->Group_mem_cont[i]=each_group_mem_cont[i];
        for(int j=0;j<each_group_mem_cont[i];j++)
            strcpy(ack->Grouplist[i][j+2],group_member_info[i][j].toUtf8());//放入用户名信息
    }
    m_tcp->write((char*)ack,sizeof(ack_str));
}
