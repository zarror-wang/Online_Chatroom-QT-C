#ifndef SP1_H
#define SP1_H

#include <QWidget>

namespace Ui {
class sp1;
}

class sp1 : public QWidget
{
    Q_OBJECT

public:
    explicit sp1(QWidget *parent = nullptr);
    ~sp1();

private:
    Ui::sp1 *ui;
};

#endif // SP1_H
