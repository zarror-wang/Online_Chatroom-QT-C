#include "client_sp0.h"
#include "client_sp1.h"
#include "ui_client_sp0.h"
#include <QMessageBox>
#include <QScrollBar>
#include <QDebug>
#include <QTcpSocket>
#include <QHostAddress>
#include <QDebug>
#include <QTest>


client_sp0::client_sp0(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::client_sp0)
{
    ui->setupUi(this);
    QImage* img=new QImage;
    img->load("/home/zarror/images/sp0.png");
    ui->pic->setPixmap(QPixmap::fromImage(*img));
    ui->name_val->setText("1120180001");
    ui->passwd_val->setText("123");
    c_tcp=new QTcpSocket;
    QString ip="127.0.0.1";//本地测试ip
    QString port="9999";//链接端口
    c_tcp->connectToHost(QHostAddress(ip),(unsigned short)port.toUShort());
    qDebug()<<"sp0申请链接"<<endl;
    //槽函数，处理服务器返回信息处理逻辑
    connect(c_tcp,&QTcpSocket::readyRead,this,[=](){
        ack=new ack_str;
        c_tcp->read((char*)ack,sizeof(ack_str));
        qDebug()<<"sp0收到恢复，回复号为"<<ack->server_type;
        fun_id_author(ack);
    });
    connect(c_tcp,&QTcpSocket::disconnected,this,[=](){
        qDebug()<<"sp0断开链接";
        c_tcp->close();
        c_tcp->deleteLater();
    });
}

client_sp0::~client_sp0()
{
    delete ui;
}


void client_sp0::on_loadin_button_clicked()
{
    clienter_id=ui->name_val->text();
    QString passwd=ui->passwd_val->text();
    qDebug()<<"请求信息"<<clienter_id<<passwd;
    req=new req_str;
    req->server_type=1;
    strcpy(req->user_id,clienter_id.toUtf8());
    strcpy(req->user_passwd,passwd.toUtf8());
    c_tcp->write((char*)req,sizeof (req_str));
}

void client_sp0::fun_id_author(ack_str* ack)//处理验证逻辑
{
    if(ack->log_flag==1)
    {
        //:跳转到用户好友界面
        c_tcp->disconnect();//todo关闭信号槽
        QString client_name_temp;
        client_name_temp.append(ack->client_name);
        qDebug()<<client_name_temp<<clienter_id;
        client_sp1* sp1=new client_sp1(nullptr,client_name_temp,clienter_id,c_tcp);
        sp1->gen_req4_userlist();//生成好友信息列表
        while(1)//等待初始化完毕
            if(sp1->init_flag=1)
                break;
        sp1->show();
        this->hide();
    }
    else
    {
        QMessageBox::critical(this,"密码错误","请重新校验账号或密码");
    }

}



