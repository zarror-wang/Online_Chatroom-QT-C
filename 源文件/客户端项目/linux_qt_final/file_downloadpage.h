#ifndef FILE_DOWNLOADPAGE_H
#define FILE_DOWNLOADPAGE_H

#include <QDialog>
#include <QTcpSocket>
#include "client_tcp_module.h"

namespace Ui {
class file_downloadpage;
}

class file_downloadpage : public QDialog
{
    Q_OBJECT

public:
    explicit file_downloadpage(QWidget *parent = nullptr,QString file_name=nullptr,QString u_name=nullptr,QString meg_seq=nullptr,QTcpSocket* c_tcp=nullptr);
    ~file_downloadpage();
    void gen_file(ack_str* ack);//生成文件

private slots:
    void on_down_load_clicked();

private:
    Ui::file_downloadpage *ui;
    QString file_name;//文件名
    QString u_name;//接收方id
    QString meg_seq;//文件消息序列号
    QByteArray buffer;//存储文件的缓冲区
    QTcpSocket* c_tcp;//套接字端口
    req_str* req;
    ack_str* ack;


};

#endif // FILE_DOWNLOADPAGE_H
