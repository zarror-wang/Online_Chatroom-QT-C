#include "client_sp1.h"
#include "ui_client_sp1.h"
#include "useritem.h"
#include "groupitem.h"
#include <QListWidget>
#include <QListWidgetItem>
#include <QVector>
#include <QMap>
#include <QString>
#include <QScrollBar>
#include <QDebug>
#include <QTcpSocket>
#include <QHostAddress>
#include <QDebug>


QVector<QPair<QString,QString>>userlist;//存储用户好友信息 首位名字，后为id
QVector<QPair<QString,QString>> grouplist;//群聊信息，包括群聊名称，群聊id
QVector<QVector<QString>> group_member_info;//每个群聊包含的用户的姓名

client_sp1::client_sp1(QWidget *parent,QString clienter_name,QString clienter_id,QTcpSocket* c_tcp_init) :
    QWidget(parent),
    clienter_name(clienter_name),
    clienter_id(clienter_id),
    c_tcp(c_tcp_init),
    ui(new Ui::client_sp1)
{
    ui->setupUi(this);
    QImage* img=new QImage;
    img->load("/home/zarror/images/sp0_1.png");
    ui->pic->setPixmap(QPixmap::fromImage(*img));
    img->load("/home/zarror/images/sp1_2.png");
    ui->sufix->setPixmap(QPixmap::fromImage(*img));
    //槽函数，处理服务器返回信息处理逻辑
    connect(c_tcp,&QTcpSocket::readyRead,this,[=](){
        //QByteArray data=c_tcp->readAll();
       // qDebug()<<"从server接收到数据---"<<data;
        ack=new ack_str;
        c_tcp->read((char*)ack,sizeof(ack_str));
        qDebug()<<"收到恢复，回复号为"<<ack->server_type;
        if(ack->server_type==2)
        {
            fun_got_userlist(ack);
            gen_req4_grouplist();
            //qDebug()<<"申请群聊列表";
        }
        else if(ack->server_type==3)
        {
           fun_got_grouplist(ack);
           qDebug()<<"sp1初始化";
           sp1_Init();
        }
    });
    connect(c_tcp,&QTcpSocket::disconnected,this,[=](){
        qDebug()<<"sp1断开链接";
        c_tcp->close();
        c_tcp->deleteLater();
    });

}

void client_sp1::sp1_Init()
{
    ui->UID_val->setText(clienter_id);
    ui->Usr_name_val->setText(clienter_name);
    UserList_Init();
    UserGroup_Init();
    init_flag=1;
}

void client_sp1::UserList_Init()//初始化好友列表
{

    //初始化好友列表部分
    QImage* img=new QImage;
    img->load("/home/zarror/images/icon_user.png");
    for(int i=0;i<userlist.size();i++)
    {
        QListWidgetItem* item=new QListWidgetItem;
        item->setSizeHint(QSize(300,50));
        useritem* uitem=new useritem(nullptr,userlist[i].first,userlist[i].second,clienter_id,clienter_name,c_tcp);
        uitem->init_useritem(img);
        ui->UserList->addItem(item);
        ui->UserList->setItemWidget(item,uitem);
    }

}

void client_sp1::UserGroup_Init()//初始化群聊列表
{

    //初始化群组列表部分
    QImage* img=new QImage;
    img->load("/home/zarror/images/icon_group_30.png");
    for(int i=0;i<grouplist.size();i++)
    {
        QListWidgetItem* item=new QListWidgetItem;
        item->setSizeHint(QSize(300,50));
        groupitem* gitem=new groupitem(nullptr,clienter_id,clienter_name, grouplist[i].first,grouplist[i].second,group_member_info[i].size(),&group_member_info[i],c_tcp);
        gitem->init_groupitem(img);
        ui->Group_List->addItem(item);
        ui->Group_List->setItemWidget(item,gitem);
    }
}

void client_sp1::gen_req4_userlist()//生成对服务器用户好友列表的请求
{
    req=new req_str;
    req->server_type=2;
    strcpy(req->user_id,clienter_id.toUtf8());
    c_tcp->write((char*)req,sizeof(req_str));
}
void client_sp1::gen_req4_grouplist()//生成对服务器群聊列表的请求
{
    req=new req_str;
    req->server_type=3;
    strcpy(req->user_id,clienter_id.toUtf8());
    c_tcp->write((char*)req,sizeof(req_str));

}

void client_sp1::fun_got_userlist(ack_str *ack)//处理服务器返回的用户好友列表信息
{
    userlist.clear();
    for(int i=0;i<ack->user_cont;i++)
        userlist.push_back(QPair<QString,QString>(ack->userlist[i][0],ack->userlist[i][1]));
}

void client_sp1::fun_got_grouplist(ack_str* ack)//处理服务器返回的群聊列表信息
{
    grouplist.clear();
    group_member_info.clear();
    QVector<QString> group_mem_temp;//临时存储群组成员信息
    for(int i=0;i<ack->Group_cont;i++)
    {
        grouplist.push_back(QPair<QString,QString>(ack->Grouplist[i][0],ack->Grouplist[i][1]));//存储群聊信息
        for(int j=0;j<ack->Group_mem_cont[i];j++)
        {
            group_mem_temp.push_back(ack->Grouplist[i][j+2]);
        }
        group_member_info.push_back(group_mem_temp);//存储群聊成员信息
        group_mem_temp.clear();
    }

}



client_sp1::~client_sp1()
{
    delete ui;
}
