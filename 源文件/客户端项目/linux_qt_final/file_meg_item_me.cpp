#include "file_meg_item_me.h"
#include "ui_file_meg_item_me.h"
#include "file_downloadpage.h"

File_meg_item_me::File_meg_item_me(QWidget *parent,QString clienter_name,QString file_name,QString meg_seq,QTcpSocket* c_tcp) :
    QWidget(parent),
    clienter_name(clienter_name),
    file_name(file_name),
    meg_seq(meg_seq),
    c_tcp(c_tcp),
    ui(new Ui::File_meg_item_me)
{
    ui->setupUi(this);
}

File_meg_item_me::~File_meg_item_me()
{
    delete ui;
}

void File_meg_item_me::init_fileitem(QImage* user_icon)
{
    ui->name_val->setText(clienter_name);
    ui->pic->setPixmap(QPixmap::fromImage(*user_icon));
    ui->file_name->setText(file_name);
}

void File_meg_item_me::on_download_clicked()//跳转到下载页面
{
    c_tcp->disconnect();//断开sp2链接
    file_downloadpage *file_download=new file_downloadpage(nullptr,file_name,clienter_name,meg_seq,c_tcp);
    file_download->show();

}
