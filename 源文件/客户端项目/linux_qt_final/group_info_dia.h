#ifndef GROUP_INFO_DIA_H
#define GROUP_INFO_DIA_H

#include <QDialog>
#include <QVector>

namespace Ui {
class group_info_dia;
}

class group_info_dia : public QDialog
{
    Q_OBJECT

public:
    explicit group_info_dia(QWidget *parent = nullptr,QString group_id=nullptr,QString group_name=nullptr,int group_mem_cont=0,QVector<QString>* group_mem=nullptr);
    ~group_info_dia();

private:
    Ui::group_info_dia *ui;
    QVector<QString>* group_mem;
    QString group_id;
    QString group_name;
    int group_mem_cont;
};

#endif // GROUP_INFO_DIA_H
