#ifndef GROUPITEM_H
#define GROUPITEM_H

#include <QWidget>
#include <QVector>
#include <QPair>
#include <QTcpSocket>

namespace Ui {
class groupitem;
}

class groupitem : public QWidget
{
    Q_OBJECT

public:
    explicit groupitem(QWidget *parent = nullptr,QString clienter_id=nullptr,QString clienter_name=nullptr,QString group_name=nullptr,QString group_id=nullptr,int group_mem_cont=0,QVector<QString>* group_mem=nullptr,QTcpSocket* c_tcp=nullptr);
    ~groupitem();
    void init_groupitem(QImage* user_icon);

private slots:
    void on_to_info_detial_clicked();

    void on_to_meg_page_clicked();

private:
    Ui::groupitem *ui;
    QString clienter_id;
    QString clienter_name;
    QString group_name;
    QString group_id;
    int group_mem_cont;
    QVector<QString>* group_mem;
    QTcpSocket* c_tcp;
};

#endif // GROUPITEM_H
