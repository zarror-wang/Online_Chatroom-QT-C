#include "normal_meg_item.h"
#include "ui_normal_meg_item.h"

normal_meg_item::normal_meg_item(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::normal_meg_item)
{
    ui->setupUi(this);
}

normal_meg_item::~normal_meg_item()
{
    delete ui;
}

void normal_meg_item::init_normalmegitem(QImage* user_icon,QString name,QString meg_con){
    ui->name_val->setText(name);
    ui->pic->setPixmap(QPixmap::fromImage(*user_icon));
    ui->meg_con->setText(meg_con);
}
