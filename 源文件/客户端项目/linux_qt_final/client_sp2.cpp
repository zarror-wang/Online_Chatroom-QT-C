#pragma once
#include "client_sp2.h"
#include "ui_client_sp2.h"
#include "client_sp1.h"
#include "normal_meg_item.h"
#include "file_meg_item.h"
#include "normal_meg_item_me.h"
#include "file_meg_item_me.h"
#include "file_meg_item.h"
#include <QDateTime>
#include <QTime>
#include <QScrollBar>
#include <QDebug>
#include <QTcpSocket>
#include <QHostAddress>
#include <QDebug>
#include <QMessageBox>
#include <QTest>
#include <QFileDialog>


QPair<QPair<int,int> ,QPair<QString,int>> dir_con;//int,int 代表信息方向<1代表别人发的，2代表自己发的>,信息类型<1代表普通消息，2代表文件传输> 后者代表内容或文件名<根据前者而定>，信息标志<1代表已经显示，2代表未显示>
QVector<QPair<QPair<int,int>,QPair<QString,int>>> solo_chat;//私人聊天窗口信息记录
QMap<QString,QString> File_name2seq;//记录文件名与文件消息的对应关系
int client_msg_cont=0;//记录服务器端的消息缓存数量


client_sp2::client_sp2(QWidget *parent,QString u_id,QString u_name,QString clienter_id,QString clienter_name,QTcpSocket* c_tcp) :
    QWidget(parent),
    u_id(u_id),
    u_name(u_name),
    clienter_name(clienter_id),
    clienter_id(clienter_name),
    c_tcp(c_tcp),
    ui(new Ui::client_sp2)
{
    ui->setupUi(this);
    ui->u_name->setText(u_name);
    connect(c_tcp,&QTcpSocket::readyRead,this,[=](){
        //QByteArray data=c_tcp->readAll();
        // qDebug()<<"从server接收到数据---"<<data;
        ack=new ack_str;
        c_tcp->read((char*)ack,sizeof(ack_str));
        qDebug()<<"收到聊天记录回复"<<ack->server_type;
        if(ack->server_type==4)//为请求信息记录的返回信息
            fun_got_meglog(ack);
        else if(ack->server_type==5)//为发送普通消息的确认信息
        {
            if(ack->insert_normal_flag==1)//消息插入数据库成功，发送成功
                QMessageBox::critical(this,"发送成功","信息发送成功");
            else//发送失败
                QMessageBox::critical(this,"发送失败","请检查网络链接");
            gen_req4_meglog();
        }
        else if(ack->server_type==6)//为发送文件消息的确认信息
        {
            if(ack->insert_file_flag==1)//消息插入数据库成功，发送成功
                QMessageBox::critical(this,"发送成功","文件发送成功");
            else//发送失败
                QMessageBox::critical(this,"发送失败","请检查网络链接");
            gen_req4_meglog();
        }
    });
    connect(c_tcp,&QTcpSocket::disconnected,this,[=](){
        qDebug()<<"sp2断开链接";
        c_tcp->close();
        c_tcp->deleteLater();
    });
}

client_sp2::~client_sp2()
{
    delete ui;
}

void client_sp2::gen_req4_meglog()//获取与当前用户的聊天记录
{
    req=new req_str;
    req->server_type=4;//请求服务类型为4
    strcpy(req->user_id,clienter_id.toUtf8());
    strcpy(req->log_tar_u_id,u_id.toUtf8());
    qDebug()<<"发出请求聊天记录请求";
    c_tcp->write((char*)req,sizeof(req_str));
}

void client_sp2::fun_got_meglog(ack_str* ack)//处理服务器返回的历史消息记录
{
    int msg_type[100]={0};//记录消息的类型
    QVector<QPair<QString,QString>> meg_list;//记录消息的(时间，内容)（文件名，文件消息序列号）
    int server_msg_cont=ack->user_meglog_cont;//记录当前状态下消息的数量
     qDebug()<<"收到回复的消息数量"<<server_msg_cont;
    //转存消息类型表
    for(int i=0;i<server_msg_cont;i++)
        msg_type[i]=ack->user_meglog_type[i];
    //转存消息
    for(int i=0;i<server_msg_cont;i++)
        meg_list.push_back(QPair<QString,QString>(ack->user_meglog[i][0],ack->user_meglog[i][1]));//[i][0]时间辍或文件名，[i][1]消息内容或文件消息序列号
    if(server_msg_cont>client_msg_cont)//服务器端的消息数量比客户端多，代表受到了新的消息，或发出了新的消息
        Update_Meg(server_msg_cont,msg_type,&meg_list);
    //发出下次刷新请求
    QTest::qSleep(500);//每间隔500ms刷新
    gen_req4_meglog();
}

void client_sp2::Update_Meg(int server_msg_cont,int meg_type[100], QVector<QPair<QString,QString>>* meg_list)//更新当前页面信息展示
{
    //将新的消息缓存到本地全局变量
    for(int i=client_msg_cont;i<server_msg_cont;i++)
        if(meg_type[i]==1)//1代表(A-B)别人发的正常消息
        {
            QPair<int,int> con1(1,1);//配饰数据域一
            QPair<QString,int> con2(meg_list->value(i).second,1);//配置数据与二
            solo_chat.push_back(QPair<QPair<int,int> ,QPair<QString,int>>(con1,con2));
        }
        else if(meg_type[i]==2)//2代表(B-A)自己发的正常消息
        {
            QPair<int,int> con1(2,1);//配饰数据域一
            QPair<QString,int> con2(meg_list->value(i).second,1);//配置数据与二
            solo_chat.push_back(QPair<QPair<int,int> ,QPair<QString,int>>(con1,con2));
        }
        else if(meg_type[i]==3)//3代表(A-B)别人发的文件
        {
            QPair<int,int> con1(1,2);//配饰数据域一
            QPair<QString,int> con2(meg_list->value(i).first,1);//配置数据与二
            File_name2seq[meg_list->value(i).first]=meg_list->value(i).second;
            solo_chat.push_back(QPair<QPair<int,int> ,QPair<QString,int>>(con1,con2));
        }
        else if(meg_type[i]==4)//4代表(B-A)自己发的文件
        {
            QPair<int,int> con1(2,2);//配饰数据域一
            QPair<QString,int> con2(meg_list->value(i).first,1);//配置数据与二
            File_name2seq[meg_list->value(i).first]=meg_list->value(i).second;
            solo_chat.push_back(QPair<QPair<int,int> ,QPair<QString,int>>(con1,con2));
        }
    client_msg_cont=server_msg_cont;//数量同步
    //将服务器端的消息本地化，之后重新梳理刷新逻辑
    QImage* img=new QImage;
    img->load("/home/zarror/images/icon_user.png");

    for(int i=0;i<solo_chat.size();i++)
    {
        if(solo_chat[i].first.first==1 && solo_chat[i].first.second==1 && solo_chat[i].second.second==1)//别人发的消息
        {
            solo_chat[i].second.second=2;//标记为已经发送
            QListWidgetItem* item=new QListWidgetItem;
            item->setSizeHint(QSize(700,60));
            normal_meg_item* nitem=new normal_meg_item;
            nitem->init_normalmegitem(img,u_name,solo_chat[i].second.first);
            ui->main_chat->addItem(item);
            ui->main_chat->setItemWidget(item,nitem);
        }
        else if(solo_chat[i].first.first==1 && solo_chat[i].first.second==2 && solo_chat[i].second.second==1)//别人发的文件
        {
            solo_chat[i].second.second=2;//标记为已经发送
            QListWidgetItem* item=new QListWidgetItem;
            item->setSizeHint(QSize(700,60));
            file_meg_item* fitem=new file_meg_item(nullptr,u_name,solo_chat[i].second.first,File_name2seq[solo_chat[i].second.first],c_tcp);
            fitem->init_fileitem(img,u_name);
            ui->main_chat->addItem(item);
            ui->main_chat->setItemWidget(item,fitem);
        }
        else if(solo_chat[i].first.first==2 && solo_chat[i].first.second==1 && solo_chat[i].second.second==1)//自己发的消息
        {
            solo_chat[i].second.second=2;//标记为已经发送
            QListWidgetItem* item=new QListWidgetItem;
            item->setSizeHint(QSize(700,60));
            normal_meg_item_me* nitem=new normal_meg_item_me;
            nitem->init_normalmegitem_me(img,"我",solo_chat[i].second.first);
            ui->main_chat->addItem(item);
            ui->main_chat->setItemWidget(item,nitem);
        }
        else if(solo_chat[i].first.first==2 && solo_chat[i].first.second==2 && solo_chat[i].second.second==1)//自己发的文件
        {
            solo_chat[i].second.second=2;//标记为已发送
            QListWidgetItem* item=new QListWidgetItem;
            item->setSizeHint(QSize(700,60));
            File_meg_item_me* fitem_me=new File_meg_item_me(nullptr,clienter_name,solo_chat[i].second.first,File_name2seq[solo_chat[i].second.first],c_tcp);
            fitem_me->init_fileitem(img);
            ui->main_chat->addItem(item);
            ui->main_chat->setItemWidget(item,fitem_me);
        }
    }
}

void client_sp2::on_back2main_clicked()//返回主界面
{
    c_tcp->disconnect();//todo关闭信号槽
    client_msg_cont=0;
    solo_chat.clear();
    client_sp1* sp1=new client_sp1(nullptr,clienter_name,clienter_id,c_tcp);
    sp1->gen_req4_userlist();//生成好友信息列表
    while(1)//等待初始化完毕
        if(sp1->init_flag=1)
            break;
    sp1->show();
    this->hide();
}

void client_sp2::on_Send_Button_clicked()//发送按钮点击，发送消息
{
    QString meg_temp="";
    meg_temp=ui->send_window->toPlainText();
    ui->send_window->clear();
    req=new req_str;
    //发送到数据库
    QDateTime* cur_time = new QDateTime(QDateTime::currentDateTime());
    QString meg_time_stamp=cur_time->toString("yyyyMMddhhmmss");//获取到该条消息的生成时间
    req->server_type=5;//请求服务的类信号为5
    strcpy(req->user_id,clienter_id.toUtf8());//记录发送方id
    strcpy(req->log_tar_u_id,u_id.toUtf8());//记录接受方id
    strcpy(req->normal_meg,meg_temp.toUtf8());//记录消息内容
    strcpy(req->time_stamp,meg_time_stamp.toUtf8());//消息时间
    qDebug()<<"发出发送消息请求"<<"时间辍大小"<<sizeof (meg_time_stamp);
    c_tcp->write((char*)req,sizeof(req_str));
}

void client_sp2::on_send_file_button_clicked()//发送文件按钮点击，发送文件
{
    QString buffer,file_name;
    QFileInfo file_info;//从路径中提炼文件信息
    //选择文件路径
    QString filepath=QFileDialog::getOpenFileName(this,"请选择文件!","/home/zarror");
    qDebug()<<filepath;
    QFile file(filepath);
    file.open(QIODevice::ReadOnly);
    buffer=file.readAll();
    file.close();
    file_info=QFileInfo(filepath);
    file_name=file_info.fileName();//得到文件名
    req=new req_str;
    //发送到数据库
    QDateTime* cur_time = new QDateTime(QDateTime::currentDateTime());
    QString meg_time_stamp=cur_time->toString("yyyyMMddhhmmss");//获取到该条消息的生成时间
    req->server_type=6;//请求服务的类信号为6
    strcpy(req->user_id,clienter_id.toUtf8());//记录发送方id
    strcpy(req->log_tar_u_id,u_id.toUtf8());//记录接受方id
    strcpy(req->file_meg,buffer.toUtf8());//记录文件内容
    strcpy(req->time_stamp,meg_time_stamp.toUtf8());//消息时间
    strcpy(req->file_name,file_name.toUtf8());//文件名
    qDebug()<<"发出发文件请求";
    c_tcp->write((char*)req,sizeof(req_str));
}

void client_sp2::fun_ctcp_recon()//重新建立tcp链接
{
    connect(c_tcp,&QTcpSocket::readyRead,this,[=](){
        //QByteArray data=c_tcp->readAll();
        // qDebug()<<"从server接收到数据---"<<data;
        ack=new ack_str;
        c_tcp->read((char*)ack,sizeof(ack_str));
        qDebug()<<"收到聊天记录回复"<<ack->server_type;
        if(ack->server_type==4)//为请求信息记录的返回信息
            fun_got_meglog(ack);
        else if(ack->server_type==5)//为发送普通消息的确认信息
        {
            if(ack->insert_normal_flag==1)//消息插入数据库成功，发送成功
                QMessageBox::critical(this,"发送成功","信息发送成功");
            else//发送失败
                QMessageBox::critical(this,"发送失败","请检查网络链接");
            gen_req4_meglog();
        }
        else if(ack->server_type==6)//为发送文件消息的确认信息
        {
            if(ack->insert_file_flag==1)//消息插入数据库成功，发送成功
                QMessageBox::critical(this,"发送成功","文件发送成功");
            else//发送失败
                QMessageBox::critical(this,"发送失败","请检查网络链接");
            gen_req4_meglog();
        }
    });
    connect(c_tcp,&QTcpSocket::disconnected,this,[=](){
        qDebug()<<"sp2断开链接";
        c_tcp->close();
        c_tcp->deleteLater();
    });

}
