#pragma once
#ifndef CLIENT_SP2_H
#define CLIENT_SP2_H

#include <QWidget>
#include <QString>
#include <QScrollBar>
#include <QDebug>
#include<QTcpSocket>
#include<QHostAddress>
#include<QDebug>
#include "client_tcp_module.h"
#include <QDateTime>
#include <QTime>
namespace Ui {
class client_sp2;
}


class client_sp2 : public QWidget
{
    Q_OBJECT

public:
    explicit client_sp2(QWidget *parent = nullptr,QString u_id=nullptr,QString u_name=nullptr,QString clienter_id=nullptr,QString clienter_name=nullptr,QTcpSocket* c_tcp=nullptr);
    ~client_sp2();
    void Update_Meg(int server_msg_cont,int meg_type[100], QVector<QPair<QString,QString>>* meg_list);//涉及到与服务器访问到的数据进行对比
    void gen_req4_meglog();//获取与当前用户的聊天记录
    void fun_got_meglog(ack_str* ack);//处理服务器返回的历史消息记录
    void fun_ctcp_recon();//重新建立tcp链接


private slots:
    void on_back2main_clicked();
    void on_Send_Button_clicked();

    void on_send_file_button_clicked();

private:
    Ui::client_sp2 *ui;
    QString u_id;//记录当前聊天页面的聊天对象id
    QString u_name;//记录当前聊天页面的聊天对象name
    QString clienter_id;//记录当前聊天页面的主id
    QString clienter_name;//记录当前聊天页面的主name
    req_str* req;
    ack_str* ack;
    QTcpSocket* c_tcp;
};

#endif // CLIENT_SP2_H
