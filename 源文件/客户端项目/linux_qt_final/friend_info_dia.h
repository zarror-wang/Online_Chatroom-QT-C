#ifndef FRIEND_INFO_DIA_H
#define FRIEND_INFO_DIA_H

#include <QDialog>

namespace Ui {
class friend_info_dia;
}

class friend_info_dia : public QDialog
{
    Q_OBJECT

public:
    explicit friend_info_dia(QWidget *parent = nullptr,QString u_id=nullptr,QString u_name=nullptr,QString u_motto=nullptr);
    ~friend_info_dia();

private:
    Ui::friend_info_dia *ui;
    QString u_id;
    QString u_name;
    QString u_motto;
};

#endif // FRIEND_INFO_DIA_H
