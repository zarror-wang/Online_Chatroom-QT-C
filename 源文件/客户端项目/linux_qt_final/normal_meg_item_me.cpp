#include "normal_meg_item_me.h"
#include "ui_normal_meg_item_me.h"

normal_meg_item_me::normal_meg_item_me(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::normal_meg_item_me)
{
    ui->setupUi(this);
}

normal_meg_item_me::~normal_meg_item_me()
{
    delete ui;
}

void normal_meg_item_me::init_normalmegitem_me(QImage* user_icon,QString name,QString meg_con){
    ui->name_val->setText(name);
    ui->pic->setPixmap(QPixmap::fromImage(*user_icon));
    ui->meg_con->setText(meg_con);
}
