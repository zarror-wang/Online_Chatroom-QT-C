#include "groupitem.h"
#include "ui_groupitem.h"
#include "client_sp3.h"
#include "group_info_dia.h"
#include "client_sp1.h"
#include <qdebug.h>

groupitem::groupitem(QWidget *parent,QString clienter_id,QString clienter_name,QString group_name,QString group_id,int group_mem_cont,QVector<QString>* group_mem,QTcpSocket* c_tcp) :
    QWidget(parent),
    clienter_id(clienter_id),
    clienter_name(clienter_name),
    group_name(group_name),
    group_id(group_id),
    group_mem_cont(group_mem_cont),
    group_mem(group_mem),
    c_tcp(c_tcp),
    ui(new Ui::groupitem)
{
    ui->setupUi(this);
}

groupitem::~groupitem()
{
    delete ui;
}

void groupitem::init_groupitem(QImage* user_icon)
{

    ui->group_name->setText(group_name);
    ui->group_icon->setPixmap(QPixmap::fromImage(*user_icon));
}


void groupitem::on_to_info_detial_clicked()
{
    //转到群聊详细信息页面
    //@todo:在这里从数据库获取详细的群聊信息
    group_info_dia * group_info=new group_info_dia(nullptr,group_id,group_name,group_mem_cont,group_mem);
    //调用函数填充
    group_info->show();
}


void groupitem::on_to_meg_page_clicked()
{
    //转到详细的聊天页面
    client_sp3* sp3=new client_sp3(nullptr,clienter_name,clienter_id,group_name,group_id,group_mem_cont,group_mem,c_tcp);
    sp3->show();
    //退出sp1页面
    ((client_sp1 *)this->parent()->parent()->parent())->hide();
    //delete this->parent()->parent()->parent();

}
