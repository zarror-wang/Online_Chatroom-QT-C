#include "file_meg_item.h"
#include "ui_file_meg_item.h"
#include "file_downloadpage.h"
#include "client_sp2.h"

file_meg_item::file_meg_item(QWidget *parent,QString u_name,QString file_name, QString meg_seq,QTcpSocket* c_tcp) :
    QWidget(parent),
    u_name(u_name),
    file_name(file_name),
    meg_seq(meg_seq),
    c_tcp(c_tcp),
    ui(new Ui::file_meg_item)
{
    ui->setupUi(this);
}

file_meg_item::~file_meg_item()
{
    delete ui;
}

void file_meg_item::init_fileitem(QImage* user_icon,QString name)
{
    ui->name_val->setText(name);
    ui->pic->setPixmap(QPixmap::fromImage(*user_icon));
    ui->file_name->setText(file_name);
}

void file_meg_item::on_download_clicked()
{
    c_tcp->disconnect();//断开sp2链接
    file_downloadpage *file_download=new file_downloadpage(nullptr,file_name,u_name,meg_seq,c_tcp);
    file_download->show();

}
