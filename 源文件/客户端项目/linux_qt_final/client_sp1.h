#pragma once
#ifndef CLIENT_SP1_H
#define CLIENT_SP1_H
#include <QWidget>
#include<QListWidget>
#include<QListWidgetItem>
#include <QScrollBar>
#include <QDebug>
#include<QTcpSocket>
#include<QHostAddress>
#include<QDebug>
#include "client_tcp_module.h"


namespace Ui {
class client_sp1;
}


class client_sp1 : public QWidget
{
    Q_OBJECT

public:
    explicit client_sp1(QWidget *parent = nullptr,QString clienter_name=nullptr,QString clienter_id=nullptr,QTcpSocket* c_tcp_init=nullptr);
    ~client_sp1();
    void gen_req4_userlist();//生成对服务器用户好友列表的请求
    void gen_req4_grouplist();//生成对服务器群聊列表的请求
    void sp1_Init();//初始化sp1页面
    void UserList_Init();//初始化好友列表
    void UserGroup_Init();//初始化群聊列表
    void fun_got_userlist(ack_str* ack);//处理服务器返回的用户好友列表信息
    void fun_got_grouplist(ack_str* ack);//处理服务器返回的群聊列表信息
    QString clienter_id;
    QString clienter_name;
    int init_flag=0;
private:
    Ui::client_sp1 *ui;
    QTcpSocket* c_tcp;
    ack_str* ack;
    req_str* req;


};

#endif // CLIENT_SP1_H
