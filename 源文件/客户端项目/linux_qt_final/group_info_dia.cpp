#include "group_info_dia.h"
#include "ui_group_info_dia.h"

group_info_dia::group_info_dia(QWidget *parent,QString group_id,QString group_name,int group_mem_cont,QVector<QString>* group_mem) :
    QDialog(parent),
    group_id(group_id),
    group_name(group_name),
    group_mem_cont(group_mem_cont),
    group_mem(group_mem),
    ui(new Ui::group_info_dia)
{
    ui->setupUi(this);
    QImage* img=new QImage;
    img->load("/home/zarror/group_info_icon.png");
    ui->group_info_icon ->setPixmap(QPixmap::fromImage(*img));
    ui->group_id_val->setText(group_id);
    ui->group_name_val->setText(group_name);
    QString str_temp;
    str_temp= str_temp.setNum(group_mem_cont,10);
    ui->group_memcont_val->setText(str_temp+"人");
    QString group_mem_temp="";
    for(int i=0;i<group_mem->size()-1;i++)
       {
        group_mem_temp+=group_mem->value(i);
        group_mem_temp+=", ";
       }
    group_mem_temp+=group_mem->value(group_mem->size()-1);
    ui->group_mem_info->setText(group_mem_temp);
    ui->group_info_val->setText("此群由Zarror于2021年7月10日建立");


}

group_info_dia::~group_info_dia()
{
    delete ui;
}
