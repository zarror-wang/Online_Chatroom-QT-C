#include "client_sp3.h"
#include "ui_client_sp3.h"
#include "client_sp1.h"
#include "normal_meg_item.h"
#include "file_meg_item.h"
#include "normal_meg_item_me.h"
#include "file_meg_item.h"
#include <QScrollBar>
#include <QDebug>
#include <QTcpSocket>
#include <QHostAddress>
#include <QDebug>
#include <QVector>
#include <QTest>
#include <QFileDialog>
#include "file_meg_item_me.h"


QPair<QPair<int,int> ,QPair<QString,int>> dir_con_1;//int,int 代表信息方向<1代表别人发的，2代表自己发的>,信息类型<1代表普通消息，2代表文件传输> 后者代表内容或文件名<根据前者而定>，信息标志<1代表已经显示，2代表未显示>
QVector<QPair<QPair<int,int>,QPair<QString,int>>> group_chat;//群聊聊天窗口信息记录
QString u_name[7]={"mike","paul","kopl","lucy","perl","bob","rop"};


client_sp3::client_sp3(QWidget *parent,QString clienter_id,QString clienter_name,QString group_name,QString group_id,int group_mem_cont,QVector<QString>* group_mem,QTcpSocket* c_tcp) :
    QWidget(parent),
    clienter_id(clienter_id),
    clienter_name(clienter_name),
    group_name(group_name),
    group_id(group_id),
    group_mem_cont(group_mem_cont),
    group_mem(group_mem),
    c_tcp(c_tcp),
    ui(new Ui::client_sp3)
{
    ui->setupUi(this);
    Update_Meg();

}

client_sp3::~client_sp3()
{
    delete ui;
}

void client_sp3::on_back2main_clicked()//转回主界面
{
    c_tcp->disconnect();//todo关闭信号槽
    client_sp1* sp1=new client_sp1(nullptr,clienter_name,clienter_id,c_tcp);
    sp1->gen_req4_userlist();//生成好友信息列表
    while(1)//等待初始化完毕
        if(sp1->init_flag=1)
            break;
    sp1->show();
    this->hide();
}

void  client_sp3::fun_gen_meg()
{
    QPair<int,int> temp1(1,1);
    QPair<QString,int> temp2("大家好",1);
    QPair<int,int> temp3(1,1);
    QPair<QString,int> temp4("hello,everyone!",1);
    QPair<int,int> temp5(2,1);
    QPair<QString,int> temp6("朋友们好",1);
    QPair<int,int> temp7(1,1);
    QPair<QString,int> temp8("这个是我们的名单",1);
    QPair<int,int> temp9(1,2);
    QPair<QString,int> temp10("name.txt",1);
    QPair<int,int> temp11(2,1);
    QPair<QString,int> temp12("这个是我的计划书",1);
    QPair<int,int> temp13(2,2);
    QPair<QString,int> temp14("plan.txt",1);
    group_chat.push_back(QPair<QPair<int,int> ,QPair<QString,int>>(temp1,temp2));
    group_chat.push_back(QPair<QPair<int,int> ,QPair<QString,int>>(temp3,temp4));
    group_chat.push_back(QPair<QPair<int,int> ,QPair<QString,int>>(temp5,temp6));
    group_chat.push_back(QPair<QPair<int,int> ,QPair<QString,int>>(temp7,temp8));
    group_chat.push_back(QPair<QPair<int,int> ,QPair<QString,int>>(temp9,temp10));
    group_chat.push_back(QPair<QPair<int,int> ,QPair<QString,int>>(temp11,temp12));
    group_chat.push_back(QPair<QPair<int,int> ,QPair<QString,int>>(temp13,temp14));
}


void client_sp3::Update_Meg()//更新当前页面信息展示
{
    fun_gen_meg();
    //将服务器端的消息本地化，之后重新梳理刷新逻辑
    QImage* img=new QImage;
    img->load("/home/zarror/images/icon_user_1.png");

    for(int i=0;i<group_chat.size();i++)
    {
        if(group_chat[i].first.first==1 && group_chat[i].first.second==1 && group_chat[i].second.second==1)//别人发的消息
        {
            group_chat[i].second.second=2;//标记为已经发送
            QListWidgetItem* item=new QListWidgetItem;
            item->setSizeHint(QSize(700,60));
            normal_meg_item* nitem=new normal_meg_item;
            nitem->init_normalmegitem(img,u_name[i],group_chat[i].second.first);
            ui->main_chat->addItem(item);
            ui->main_chat->setItemWidget(item,nitem);
        }
        else if(group_chat[i].first.first==1 && group_chat[i].first.second==2 && group_chat[i].second.second==1)//别人发的文件
        {
            group_chat[i].second.second=2;//标记为已经发送
            QListWidgetItem* item=new QListWidgetItem;
            item->setSizeHint(QSize(700,60));
            file_meg_item* fitem=new file_meg_item(nullptr,u_name[i],group_chat[i].second.first,"24",c_tcp);
            fitem->init_fileitem(img,u_name[i]);
            ui->main_chat->addItem(item);
            ui->main_chat->setItemWidget(item,fitem);
        }
        else if(group_chat[i].first.first==2 && group_chat[i].first.second==1 && group_chat[i].second.second==1)//自己发的消息
        {
            group_chat[i].second.second=2;//标记为已经发送
            QListWidgetItem* item=new QListWidgetItem;
            item->setSizeHint(QSize(700,60));
            normal_meg_item_me* nitem=new normal_meg_item_me;
            nitem->init_normalmegitem_me(img,"我",group_chat[i].second.first);
            ui->main_chat->addItem(item);
            ui->main_chat->setItemWidget(item,nitem);
        }
        else if(group_chat[i].first.first==2 && group_chat[i].first.second==2 && group_chat[i].second.second==1)//自己发的文件
        {
            group_chat[i].second.second=2;//标记为已发送
            QListWidgetItem* item=new QListWidgetItem;
            item->setSizeHint(QSize(700,60));
            File_meg_item_me* fitem_me=new File_meg_item_me(nullptr,"我",group_chat[i].second.first,"24",c_tcp);
            fitem_me->init_fileitem(img);
            ui->main_chat->addItem(item);
            ui->main_chat->setItemWidget(item,fitem_me);
        }
    }
}




void client_sp3::on_Send_Button_clicked()//向群聊发送消息
{
    QString meg;
    QImage* img=new QImage;
    img->load("/home/zarror/icon.png");
    meg=ui->send_window->toPlainText();
    ui->send_window->clear();
    QListWidgetItem* item=new QListWidgetItem;
    item->setSizeHint(QSize(700,60));
    normal_meg_item_me* nitem=new normal_meg_item_me;
    nitem->init_normalmegitem_me(img,"我",meg);
    ui->main_chat->addItem(item);
    ui->main_chat->setItemWidget(item,nitem);



}

void client_sp3::on_send_file_clicked()
{
    QImage* img=new QImage;
    img->load("/home/zarror/icon.png");
    QString buffer,file_name;
    QFileInfo file_info;//从路径中提炼文件信息
    //选择文件路径
    QString filepath=QFileDialog::getOpenFileName(this,"请选择文件!","/home/zarror");
    qDebug()<<filepath;
    QFile file(filepath);
    file.open(QIODevice::ReadOnly);
    buffer=file.readAll();
    file.close();
    file_info=QFileInfo(filepath);
    file_name=file_info.fileName();//得到文件名
    QListWidgetItem* item=new QListWidgetItem;
    item->setSizeHint(QSize(700,60));
    File_meg_item_me* fitem_me=new File_meg_item_me(nullptr,"我","test.txt","texs.txt",c_tcp);
    fitem_me->init_fileitem(img);
    ui->main_chat->addItem(item);
    ui->main_chat->setItemWidget(item,fitem_me);

}
